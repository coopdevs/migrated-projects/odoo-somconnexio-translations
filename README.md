# Module to manage SomConnexio's ODOO translation files

This library keeps all the catalan and spanish `.PO` files for the external odoo modules that we use in our ODOO based ERP (see https://www.odoo.com/documentation/14.0/howtos/backend.html?highlight=view#internationalization)

Every update of these files or addition of new ones should be uploaded in this repository.

The translation files for the terms created within the development of our custom Som Connexió module (https://gitlab.com/coopdevs/odoo-somconnexio) are found inside the `i18n` folder.
The external `.po` files found here could be as well pushed as Merge Requests to their respective module repositories if wanted.

## Import translations

To load the translations to the ODOO server, follow these steps:


1. **Access to the server with the browser:**
  - In development visit http://odoo-sc.local:8069
  - In staging visit https://staging-odoo.somconnexio.coop
  - In pre-production visit https://sc-preprod-odoo.coopdevs.org
  - In production visit https://odoo.somconnexio.coop

2. **Enter with debug mode**

   He have two options to do it:
    - Add "debug=true" inside the URL as paramether after the question mark ("https://*base-url*/web?debug=true#action...")

    - Button `Activate the developper mode` from the Settings tab (see image below)

  ![Developper mode button](img/debug_mode.png "Developer mode button in Settings")

3. **Settings -> Translations -> Import Translation**

4. __Fill the language with name and the language code*__(see image below)

*Language code:
> locale code for the language, or the language and country combination when they differ (e.g. pt.po or pt_BR.po)

![Import translations](img/import_translation.png "Import translations")